﻿using UnityEngine;

public class RotationToTarget
{
    public Quaternion UpdateRotation(Transform fromTransform, Vector3 toPosition, float rotationSpeed)
    {
        Vector2 targetPosition = toPosition - fromTransform.position;
        float angle = Mathf.Atan2(targetPosition.y, targetPosition.x) * Mathf.Rad2Deg;

        Quaternion fromAngle = fromTransform.rotation;
        Quaternion toAngle = Quaternion.AngleAxis(angle, Vector3.forward);

        return Quaternion.Slerp(fromAngle, toAngle, Time.deltaTime * rotationSpeed);
    }
}