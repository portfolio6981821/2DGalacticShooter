﻿using UnityEngine;
using System;
using Cinemachine;

[RequireComponent(typeof(Collider2D))]
public class TargetCacher : MonoBehaviour
{
    public event Action<Transform> CachTarget;
    public event Action LostTarget;

    [SerializeField, TagField] private string _tagToCatch;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareTag(_tagToCatch))
        {
            CachTarget?.Invoke(other.transform);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.CompareTag(_tagToCatch))
        {
            LostTarget?.Invoke();
        }
    }
}
