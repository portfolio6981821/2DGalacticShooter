﻿using UnityEngine;

public class CanonPlatform : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed = 0.5f;
    [SerializeField] private TargetCacher _targetCacher;

    [SerializeField] private SeriesShooter _shooter;

    private Transform _target;
    private RotationToTarget _rotator;

    private void Awake()
    {
        _rotator = new RotationToTarget();
    }

    private void OnEnable()
    {
        _targetCacher.CachTarget += OnCatchTarget;
        _targetCacher.LostTarget += OnLostTarget;
    }

    private void OnDisable()
    {
        _targetCacher.CachTarget -= OnCatchTarget;
        _targetCacher.LostTarget -= OnLostTarget;
    }

    private void Update()
    {
        if (_target == null)
        {
            return;
        }

        transform.rotation = _rotator.UpdateRotation(transform, _target.position, _rotationSpeed);
    }

    private void OnCatchTarget(Transform target)
    {
        _target = target;
        _shooter.TryStartShooting();
    }

    private void OnLostTarget()
    {
        _target = null;
        _shooter.StopShooting();
    }
}
