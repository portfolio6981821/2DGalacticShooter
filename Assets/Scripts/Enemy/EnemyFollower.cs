﻿using UnityEngine;

[RequireComponent(typeof(HPManager))]
public class EnemyFollower : MonoBehaviour, ISpawnable
{
    private float _speed;

    private Transform _target;
    private Vector2 _targetPosition;
    private HPManager _hpManager;
    private RotationToTarget _rotator;

    private void Awake()
    {
        _hpManager = GetComponent<HPManager>();
        _rotator = new RotationToTarget();
    }

    private void OnEnable()
    {
        _hpManager.OnDeath += Death;
    }

    private void OnDisable()
    {
        _hpManager.OnDeath -= Death;
    }

    public void SetupSpeed(float speed)
    {
        _speed = speed;

        _target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if (_target != null)
        {
            _targetPosition = _target.transform.position;
        }

        transform.position = Vector2.MoveTowards(transform.position, _targetPosition, _speed * Time.deltaTime);
        transform.rotation = _rotator.UpdateRotation(transform, _targetPosition, _speed * 0.5f);
    }

    private void Death()
    {
        Destroy(gameObject);
    }
}
