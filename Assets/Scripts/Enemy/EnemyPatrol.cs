using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    [SerializeField] private Transform[] _patrolingPoints;
    [SerializeField] private float _patrolSpeed;
    [SerializeField] private float _rotationSpeed;

    private int _index = 0;
    private RotationToTarget _rotator;

    private void Awake()
    {
        _rotator = new RotationToTarget();
    }

    private void Update()
    {
        Patrol();

        transform.rotation = _rotator.UpdateRotation(transform, _patrolingPoints[_index].transform.position, _rotationSpeed);
    }

    private void Patrol()
    {
        float step = _patrolSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.right, step);

        NextStep();
    }

    private void NextStep()
    {
        if (Vector2.Distance(transform.position, _patrolingPoints[_index].position) > 1f/_rotationSpeed)
        {
            return;
        }

        if (_index + 1 == _patrolingPoints.Length)
        {
            _index = 0;
            return;
        }

        _index++;
    }
}
