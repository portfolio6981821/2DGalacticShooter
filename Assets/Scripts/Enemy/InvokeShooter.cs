using UnityEngine;

public class InvokeShooter : Shooter
{
    [SerializeField] private float _bulletSpeed;
    [SerializeField] private int _bulletDamage;
    [SerializeField] private float _timeBetweenShoots;

    private void OnEnable()
    {
        InvokeRepeating(nameof(RepeatShot), _timeBetweenShoots, _timeBetweenShoots);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void RepeatShot()
    {
        BulletVariables bulletData = new BulletVariables(_bulletSpeed, transform.up, _bulletDamage);
        Shot(bulletData);
    }
}
