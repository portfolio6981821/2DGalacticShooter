using UnityEngine;

public class PatrolingManager : MonoBehaviour
{
    [SerializeField] private HPManager _hpManager;
    [SerializeField] private InvokeShooter _shooter;
    [SerializeField] private EnemyPatrol _patrol;
    [SerializeField] private EnemyFollower _follow;

    [Space(5)]
    [SerializeField] private TargetCacher _chasingRegion;
    [SerializeField] private float _chasingSpeed;

    private void OnEnable()
    {
        _hpManager.OnDeath += Death;
        _chasingRegion.CachTarget += StartChasing;
        _chasingRegion.LostTarget += StopChasing;

        _follow.SetupSpeed(_chasingSpeed);
    }

    private void OnDisable()
    {
        _chasingRegion.CachTarget -= StartChasing;
        _chasingRegion.LostTarget -= StopChasing;
        _hpManager.OnDeath -= Death;
    }

    private void Death()
    {
        Destroy(gameObject);
    }

    private void StartChasing(Transform transform)
    {
        _follow.enabled = true;
        _shooter.enabled = true;

        _patrol.enabled = false;
    }

    private void StopChasing()
    {
        _follow.enabled = false;
        _shooter.enabled = false;

        _patrol.enabled = true;
    }
}
