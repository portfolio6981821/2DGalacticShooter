using System.Collections;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(Collider2D))]
public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject _prefabToSpawn;
    [SerializeField] private float _spawnObjectSpeed;
    [SerializeField] [Range(0.2f, 5f)] private float _spawningDelay = 0.5f;
    [SerializeField, TagField] private string _tagToActivate = "Player";

    private Coroutine _spawningCorutine;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.transform.CompareTag(_tagToActivate))
        {
            return;
        }

        TryToSpawn();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.transform.CompareTag(_tagToActivate))
        {
            return;
        }

        StopAllCoroutines();
        _spawningCorutine = null;
    }

    private IEnumerator SpawnPrefab()
    {
        float randomNumber = Random.Range(-3.3f, 3.3f);

        GameObject objectToSpawn = Instantiate(_prefabToSpawn, new Vector3(transform.position.x, randomNumber, transform.position.z - 5f), Quaternion.identity);
        objectToSpawn.transform.SetParent(gameObject.transform);

        if (objectToSpawn.TryGetComponent<ISpawnable>(out ISpawnable spawner))
        {
            spawner.SetupSpeed(_spawnObjectSpeed);
        }

        yield return new WaitForSeconds(_spawningDelay);

        _spawningCorutine = null;
        TryToSpawn();
    }

    private void TryToSpawn()
    {
        if (_spawningCorutine != null)
        {
            return;
        }

        _spawningCorutine = StartCoroutine(SpawnPrefab());
    }
}

public interface ISpawnable
{
    public void SetupSpeed(float speed);
}
