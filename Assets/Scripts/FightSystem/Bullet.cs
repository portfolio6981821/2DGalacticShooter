using UnityEngine;
using System;
using System.Collections;
using Cinemachine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private int _defaultPoolCapacity;
    [SerializeField] private int _maxPoolCapacity;
    [SerializeField, TagField] private string _obstacleTag = "Obstacle";

    private int _damage;
    private Action<Bullet> _destroyAction;

    public int DefaultPoolCapacity { get => _defaultPoolCapacity; private set => _defaultPoolCapacity = value; }
    public int MaxPoolCapacity { get => _maxPoolCapacity; private set => _maxPoolCapacity = value; }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareTag(_obstacleTag))
        {
            BulletHitEffect();
        }

        HPManager targetHP;
        if (other.TryGetComponent<HPManager>(out targetHP))
        {
            targetHP.GetHit(_damage);
            BulletHitEffect();
        }
    }

    public void Initialize(BulletVariables bulletData, Action<Bullet> destroyAction)
    {
        _destroyAction = destroyAction;

        _damage = bulletData.Damage;
        GetComponent<Rigidbody2D>().velocity = bulletData.Direction.normalized * bulletData.Speed;

        StartCoroutine(InvokeDestroy());
    }

    private IEnumerator InvokeDestroy()
    {
        yield return new WaitForSeconds(3f);
        if (gameObject.activeSelf)
        {
            BulletHitEffect();
        }
    }

    private void BulletHitEffect()
    {
        _destroyAction(this);
    }
}
