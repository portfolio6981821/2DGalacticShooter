using UnityEngine;
using System;

public class HPManager : MonoBehaviour
{
    public event Action<int> OnGetHit;
    public event Action OnDeath;

    [SerializeField] private int _maxHP;
    private int _currentHp;

    public int MaxHP { get => _maxHP; private set => _maxHP = value; }
    public int CurrentHp { get => _currentHp; private set => _currentHp = value; }

    private void Awake()
    {
        Reset();
    }

    public void GetHit(int damage)
    {
        _currentHp -= damage;

        if (_currentHp <= 0)
        {
            _currentHp = 0;
            OnDeath?.Invoke();
        }

        OnGetHit?.Invoke(_currentHp);
    }

    public void Reset()
    {
        _currentHp = MaxHP;
        OnGetHit?.Invoke(_currentHp);
    }
}
