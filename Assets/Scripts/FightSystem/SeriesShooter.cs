using System.Collections;
using UnityEngine;

public class SeriesShooter : Shooter
{
    [SerializeField, Range(1, 10)] private int _amountBulletInOneSeries = 5;
    [SerializeField, Range(0.2f, 1f)] private float _delayBetweenBullets = 0.3f;
    [SerializeField, Range(0.5f, 5f)] private float _delayBetweenSeries = 1f;

    [SerializeField] private float _bulletSpeed = 16f;
    [SerializeField] private int _bulletDamage;

    private Coroutine _shootingCorutine;

    public void TryStartShooting()
    {
        if(_shootingCorutine != null)
        {
            return;
        }

        _shootingCorutine = StartCoroutine(StartSeries());
    }

    public void StopShooting()
    {
        StopAllCoroutines();
        _shootingCorutine = null;
    }

    private IEnumerator StartSeries()
    {
        yield return new WaitForSeconds(_delayBetweenSeries);
        float randomChange;

        for (int i = 0; i < _amountBulletInOneSeries; i++)
        {
            Vector3 bulletDir = Aim.transform.position - transform.position;

            randomChange = UnityEngine.Random.Range(-0.15f, 0.15f);
            Vector3 randomDirectionChange = new Vector3(randomChange, randomChange, 0f);
            bulletDir += randomDirectionChange;

            BulletVariables bulletData = new BulletVariables(_bulletSpeed, bulletDir, _bulletDamage);
            Shot(bulletData);
            yield return new WaitForSeconds(_delayBetweenBullets);
        }

        yield return new WaitForSeconds(_delayBetweenSeries);
        _shootingCorutine = null;

        TryStartShooting();
    }
}
