﻿using UnityEngine;
using UnityEngine.Pool;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class Shooter : MonoBehaviour
{
    public static Dictionary<string, ObjectPool<Bullet>> BulletDictionary = new Dictionary<string, ObjectPool<Bullet>>();

    [SerializeField] private Bullet _bulletPrefab;
    [SerializeField] private AudioClip _shotindSound;
    [SerializeField] private Transform _aim;

    private ObjectPool<Bullet> _currentPool;
    private AudioSource _audioSource;

    public Transform Aim { get => _aim; private set => _aim = value; }

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        AddToDictionaryPool();
    }

    private void AddToDictionaryPool()
    {
        if (!BulletDictionary.ContainsKey(_bulletPrefab.gameObject.name))
        {
            _currentPool = new ObjectPool<Bullet>(() =>
            {
                return Instantiate(_bulletPrefab);
            },

            bullet =>
            {
                bullet.gameObject.SetActive(true);
            },

            bullet =>
            {
                bullet.gameObject.SetActive(false);
            },

            bullet =>
            {
                Destroy(bullet);
            },

            false, _bulletPrefab.DefaultPoolCapacity, _bulletPrefab.MaxPoolCapacity);

            BulletDictionary.Add(_bulletPrefab.name, _currentPool);
            return;
        }

        Shooter.BulletDictionary.TryGetValue(_bulletPrefab.name, out _currentPool);
    }

    public void Shot(BulletVariables bulletData)
    {
        Bullet bullet = _currentPool.Get();
        bullet.Initialize(bulletData, BackToPool);
        bullet.gameObject.transform.position = _aim.position;
        PlayAudio();
    }

    private void BackToPool(Bullet bullet)
    {
        _currentPool.Release(bullet);
    }

    private void PlayAudio()
    {
        if (_shotindSound == null)
        {
            return;
        }

        _audioSource.clip = _shotindSound;
        _audioSource.Play();
    }
}

public struct BulletVariables
{
    public float Speed { get; private set; }
    public Vector2 Direction { get; private set; }
    public int Damage { get; private set; }

    public BulletVariables(float speed, Vector2 direction, int damage)
    {
        Speed = speed;
        Direction = direction;
        Damage = damage;
    }
}
