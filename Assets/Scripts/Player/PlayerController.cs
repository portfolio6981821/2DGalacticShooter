using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private FloatSettingsVariable _moveSpeed;

    private float _currentMoveSpeed = 7f;

    private Rigidbody2D _rigidBody;
    private float _inputX, _inputY;
    private Vector2 _playerVelocity;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();

        SetupMoveSpeed(_moveSpeed.CurrentValue);
        _moveSpeed.OnValueChange += SetupMoveSpeed;
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        _rigidBody.velocity = _playerVelocity;

        Rotate();
    }

    private void Rotate()
    {
        if (_playerVelocity == Vector2.zero)
        {
            return;
        }

        float angle = Mathf.Atan2(_playerVelocity.y, _playerVelocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void OnMove(InputValue value)
    {
        _playerVelocity = value.Get<Vector2>().normalized * _currentMoveSpeed;
    }

    private void SetupMoveSpeed(float speed)
    {
        _currentMoveSpeed = speed;
    }

    private void OnDestroy()
    {
        _moveSpeed.OnValueChange -= SetupMoveSpeed;
    }
}
