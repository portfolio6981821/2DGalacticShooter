using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private BoolSettingsVariable _isGodModeOn;
    [SerializeField] private HPManager _hpManager;

    private void OnEnable()
    {
        _hpManager.OnDeath += Death;
    }

    private void OnDisable()
    {
        _hpManager.OnDeath -= Death;
    }

    private void Death()
    {
        if (_isGodModeOn.CurrentValue)
        {
            return;
        }

        Destroy(gameObject);
    }
}
