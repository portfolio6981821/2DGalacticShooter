using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerShooter : Shooter
{
    [SerializeField] private float _bulletSpeed;
    [SerializeField] private int _basicBulletDamage;

    private void OnFire(InputValue value)
    {
        if (value.isPressed)
        {
            BulletVariables bulletToShoot = new BulletVariables(_bulletSpeed, transform.right, _basicBulletDamage);
            Shot(bulletToShoot);
        }
    }
}
