using System;
using System.IO;
using UnityEngine;

public class FileDataHandler
{
    private string _dataDirectionPath;
    private string _dataFileName;

    public FileDataHandler(string dataFileName)
    {
        _dataDirectionPath = Application.persistentDataPath;
        _dataFileName = dataFileName;
    }

    public string Load()
    {
        string fullPath = Path.Combine(_dataDirectionPath, _dataFileName);

        string loadedData = null;

        if (!File.Exists(fullPath))
        {
            return loadedData;
        }

        try
        {
            FileStream stream = new FileStream(fullPath, FileMode.Open);
            StreamReader reader = new StreamReader(stream);

            using (stream)
            {
                using (reader)
                {
                    loadedData = reader.ReadToEnd();
                }
            }

            return loadedData;
        }

        catch (Exception exception)
        {
            Debug.LogError("Error occured when trying to load data from file: " + fullPath + "\n" + exception);
            throw;
        }
    }

    public void Save(string data)
    {
        string fullPath = Path.Combine(_dataDirectionPath, _dataFileName);
        try
        {
            Directory.CreateDirectory(Path.GetDirectoryName(fullPath));

            FileStream stream = new FileStream(fullPath, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            using (stream)
            {
                using (writer)
                {
                    writer.Write(data);
                }
            }
        }

        catch (Exception exception)
        {
            Debug.LogError("Error occured when trying to save data to file: " + fullPath + "\n" + exception);
        }
    }
}
