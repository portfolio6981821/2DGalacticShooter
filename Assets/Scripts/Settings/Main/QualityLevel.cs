﻿using UnityEngine;
using System.Collections.Generic;

public class QualityLevel
{
    public List<string> QualityOptions { get; private set; }

    private int _currentQualityValue;

    public QualityLevel(int qualityIndex)
    {
        GetAvailableQualityOptions();

        CurrentQualityValue = qualityIndex;
    }

    private void GetAvailableQualityOptions()
    {
        QualityOptions = new List<string>();

        foreach (string qualityName in QualitySettings.names)
        {
            QualityOptions.Add(qualityName);
        }
    }

    public int CurrentQualityValue
    {
        get => _currentQualityValue;

        set
        {
            _currentQualityValue = value;
            QualitySettings.SetQualityLevel(CurrentQualityValue);
        }
    }
}
