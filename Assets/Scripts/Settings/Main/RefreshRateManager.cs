using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RefreshRateManager
{
    [SerializeField] private List<int> _refreshRates;
    [SerializeField] private int _indexOfDefault = 0;
    private int _currentIndex = 0;

    public List<int> RefreshRates => _refreshRates;
    public int IndexOfDefault => _indexOfDefault;

    public int CurrentIndex
    {
        get
        {
            return _currentIndex;
        }

        private set
        {
            CheckIndexRange(ref value);
            _currentIndex = value;
            Application.targetFrameRate = RefreshRates[_currentIndex];
        }
    }

    public void SetActualRefreshRate(int index)
    {
        CurrentIndex = index;
    }

    private int CheckIndexRange(ref int index)
    {
        if (index > _refreshRates.Count - 1)
        {
            index = _refreshRates.Count - 1;
        }

        return index;
    }
}
