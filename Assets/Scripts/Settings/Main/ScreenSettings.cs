﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ScreenSettings
{
    public List<Vector2> Resolutions { get; private set; }
    public Vector2 CurrentResolution { get; private set; }

    public BoolSettingsVariable FullScreenSetting { get; private set; }
    public BoolSettingsVariable VSyncSetting { get; private set; }

    public ScreenSettings(BoolSettingsVariable fullScreen, BoolSettingsVariable vSync, Vector2 screenResolution)
    {
        GetAvailableResolutions();

        FullScreenSetting = fullScreen;
        VSyncSetting = vSync;

        Screen.SetResolution((int)screenResolution.x, (int)screenResolution.y, FullScreenSetting.CurrentValue);
        QualitySettings.vSyncCount = Convert.ToInt32(VSyncSetting.CurrentValue);

        CurrentResolution = screenResolution;
    }

    public void SetSreenOptions(Vector2 screenResolution, bool isFullScreen, bool isVsync)
    {
        Screen.SetResolution((int)screenResolution.x, (int)screenResolution.y, isFullScreen);
        QualitySettings.vSyncCount = Convert.ToInt32(isVsync);

        FullScreenSetting.CurrentValue = isFullScreen;
        VSyncSetting.CurrentValue = isVsync;

        CurrentResolution = screenResolution;
    }

    private void GetAvailableResolutions()
    {
        Resolutions = new List<Vector2>();

        foreach (var resolution in Screen.resolutions)
        {
            if (Resolutions.Count == 0)
            {
                Vector2 newResolution = new Vector2(resolution.width, resolution.height);
                Resolutions.Add(newResolution);
                continue;
            }

            if (!CheckIfContainsResolution(resolution))
            {
                Vector2 resolutionToadd = new Vector2(resolution.width, resolution.height);
                Resolutions.Add(resolutionToadd);
            }
        }

        Resolutions.OrderBy(t => t.x).ThenBy(t => t.y).ToList();
    }

    private bool CheckIfContainsResolution(Resolution resolutionToCheck)
    {
        Vector2 vectorToCheck = new Vector2(resolutionToCheck.width, resolutionToCheck.height);
        bool contain = Resolutions.Contains(vectorToCheck);
        return contain;
    }
}