using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "Game/Settings/Manager")]
public class SettingsManager : ScriptableObject
{
    [SerializeField] private FloatSettingsVariable[] _floatSettings;
    [SerializeField] private BoolSettingsVariable[] _boolVariables;

    [SerializeField] private RefreshRateManager _refreshRates;
    [SerializeField] private BoolSettingsVariable _isFullScreen;
    [SerializeField] private BoolSettingsVariable _isVsync;

    private ScreenSettings _screenOption;
    private QualityLevel _qualityLevel;
    private FileDataHandler _saveSystem;
    private readonly string _fileName = "Settings";

    public RefreshRateManager RefreshRates => _refreshRates;
    public QualityLevel QualityLevel => _qualityLevel;
    public ScreenSettings ScreenOption => _screenOption;

    private void OnEnable()
    {
        _saveSystem = new FileDataHandler(_fileName);

        Load();
    }

    private void OnDisable()
    {
        Save();
    }

    public void Save()
    {
        SettingsSaveData settingsToSave = new SettingsSaveData();

        settingsToSave.FloatVariables = new float[_floatSettings.Length];
        for (int i = 0; i < _floatSettings.Length; i++)
        {
            settingsToSave.FloatVariables[i] = _floatSettings[i].CurrentValue;
        }

        settingsToSave.BoolVariables = new bool[_boolVariables.Length];
        for (int i = 0; i < _boolVariables.Length; i++)
        {
            settingsToSave.BoolVariables[i] = _boolVariables[i].CurrentValue;
        }

        settingsToSave.Resolution = ScreenOption.CurrentResolution;
        settingsToSave.QualityIndex = _qualityLevel.CurrentQualityValue;
        settingsToSave.RefreshRateIndex = RefreshRates.CurrentIndex;

        string json = JsonUtility.ToJson(settingsToSave);
        _saveSystem.Save(json);
    }

    public void Load()
    {
        SettingsSaveData settingsToLoad;
        string json = _saveSystem.Load();

        if (json == null)
        {
            settingsToLoad = LoadDefault();
        }

        else
        {
            settingsToLoad = JsonUtility.FromJson<SettingsSaveData>(json);
        }

        for (int i = 0; i < settingsToLoad.FloatVariables.Length; i++)
        {
            _floatSettings[i].CurrentValue = settingsToLoad.FloatVariables[i];
        }

        for (int i = 0; i < settingsToLoad.BoolVariables.Length; i++)
        {
            _boolVariables[i].CurrentValue = settingsToLoad.BoolVariables[i];
        }

        _qualityLevel = new QualityLevel(settingsToLoad.QualityIndex);
        _screenOption = new ScreenSettings(_isFullScreen, _isVsync, settingsToLoad.Resolution);

        RefreshRates.SetActualRefreshRate(settingsToLoad.RefreshRateIndex);
    }

    private SettingsSaveData LoadDefault()
    {
        SettingsSaveData defaultSettings = new SettingsSaveData();

        defaultSettings.FloatVariables = new float[_floatSettings.Length];
        for (int i = 0; i < _floatSettings.Length; i++)
        {
            defaultSettings.FloatVariables[i] = _floatSettings[i].DefaultValue;
        }

        defaultSettings.BoolVariables = new bool[_boolVariables.Length];
        for (int i = 0; i < _boolVariables.Length; i++)
        {
            defaultSettings.BoolVariables[i] = _boolVariables[i].DefaultValue;
        }

        defaultSettings.Resolution = new Vector2(Screen.width, Screen.height);
        defaultSettings.RefreshRateIndex = _refreshRates.IndexOfDefault;
        defaultSettings.QualityIndex = QualitySettings.GetQualityLevel();

        return defaultSettings;
    }
}
