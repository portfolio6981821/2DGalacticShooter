﻿using UnityEngine;

[System.Serializable]
public struct SettingsSaveData
{
    public float[] FloatVariables;
    public bool[] BoolVariables;
    public Vector2 Resolution;
    public int QualityIndex;
    public int RefreshRateIndex;
}
