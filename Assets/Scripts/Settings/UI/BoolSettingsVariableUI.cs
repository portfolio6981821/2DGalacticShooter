using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BoolSettingsVariableUI : MonoBehaviour
{
    [SerializeField] private Toggle _uiToRead;
    [SerializeField] private TextMeshProUGUI _nameField;
    [SerializeField] private BoolSettingsVariable _value;

    private void Awake()
    {
        if (_value == null)
        {
            Debug.LogError("No value to read!!");
            return;
        }

        _nameField.text = _value.SettingName;

        _uiToRead.isOn = _value.CurrentValue;
        _uiToRead.onValueChanged.AddListener(OnValueChange);
    }

    private void OnEnable()
    {
        _uiToRead.isOn = _value.CurrentValue;
    }

    private void OnValueChange(bool value)
    {
        _value.CurrentValue = value;
    }
}
