using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class FloatSettingsUI : MonoBehaviour
{
    [SerializeField] private Slider _uiToRead;
    [SerializeField] private TextMeshProUGUI _valueField;
    [SerializeField] private TextMeshProUGUI _nameField;

    [SerializeField] private FloatSettingsVariable _value;

    private void Awake()
    {
        if (_value == null)
        {
            Debug.LogError("No value to read!!");
            return;
        }

        _nameField.text = _value.SettingName;

        _uiToRead.wholeNumbers = _value.OnlyWholeNumbers;
        _uiToRead.minValue = _value.MinimumValue;
        _uiToRead.maxValue = _value.MaximumValue;
        _uiToRead.value = _value.CurrentValue;
        UpdateText();
        _uiToRead.onValueChanged.AddListener(OnValueChange);
    }

    private void OnEnable()
    {
        _uiToRead.value = _value.CurrentValue;
        UpdateText();
    }

    private void OnValueChange(float value)
    {
        _value.CurrentValue = value;
        UpdateText();
    }

    private void UpdateText()
    {
        if (_valueField == null)
        {
            return;
        }
        _valueField.text = _value.CurrentValue.ToString();
    }
}
