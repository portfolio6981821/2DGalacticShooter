using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GraphSettingsUI : MonoBehaviour
{
    [SerializeField] private SettingsManager _settings;

    [Header("Resolution UI")]
    [SerializeField] private TMP_Text _resolutionUI;
    [SerializeField] private Button _rightArrowResolutionUI;
    [SerializeField] private Button _leftArrowResolutionUI;

    [Space(10)]
    [SerializeField] private TMP_Dropdown _qualityUI;
    [Space(10)]
    [SerializeField] private TMP_Dropdown _refreshRateUI;
    [Space(10)]
    [SerializeField] private Toggle _fullScreenUI;
    [Space(10)]
    [SerializeField] private Toggle _vSyncUI;
    [Space(10)]
    [SerializeField] private Button _applyChangesUI;

    private int _selectedResolutionIndex;
    private int _qualityValue;
    private int _refreshRateIndex;
    private bool _fullScreen;
    private bool _vSync;

    private void OnEnable()
    {
        LoadUIValue();
        SetupListeners();
    }

    private void LoadUIValue()
    {
        _fullScreenUI.isOn = _settings.ScreenOption.FullScreenSetting.CurrentValue;
        _fullScreen = _fullScreenUI.isOn;

        _qualityUI.ClearOptions();
        _qualityUI.AddOptions(_settings.QualityLevel.QualityOptions);

        _qualityValue = _settings.QualityLevel.CurrentQualityValue;
        _qualityUI.value = _qualityValue;

        _vSyncUI.isOn = _settings.ScreenOption.VSyncSetting.CurrentValue;
        _vSync = _vSyncUI.isOn;

        SetupRefreshRate();

        Vector2 currentResolution = _settings.ScreenOption.CurrentResolution;
        _selectedResolutionIndex = _settings.ScreenOption.Resolutions.IndexOf(currentResolution);

        UpdateResolutionText(currentResolution);

        _rightArrowResolutionUI.gameObject.SetActive(_selectedResolutionIndex != _settings.ScreenOption.Resolutions.Count - 1);
        _leftArrowResolutionUI.gameObject.SetActive(_selectedResolutionIndex != 0);
    }

    private void SetupRefreshRate()
    {
        _refreshRateUI.ClearOptions();

        List<string> refreshOption = new List<string>();

        foreach (var refreshRate in _settings.RefreshRates.RefreshRates)
        {
            refreshOption.Add(refreshRate.ToString());
        }

        _refreshRateUI.AddOptions(refreshOption);
        _refreshRateUI.value = _settings.RefreshRates.CurrentIndex;
    }

    private void SetupListeners()
    {
        _fullScreenUI.onValueChanged.AddListener(SetFullScreen);
        _vSyncUI.onValueChanged.AddListener(SetVSync);
        _qualityUI.onValueChanged.AddListener(SetQuality);

        _rightArrowResolutionUI.onClick.AddListener(ResolutionButtonRight);
        _leftArrowResolutionUI.onClick.AddListener(ResolutionButtonLeft);
        _applyChangesUI.onClick.AddListener(ApplyGraphicChange);
        _refreshRateUI.onValueChanged.AddListener(SetRefreshRate);
    }

    private void ApplyGraphicChange()
    {
        _settings.QualityLevel.CurrentQualityValue = _qualityValue;

        int width = (int)_settings.ScreenOption.Resolutions[_selectedResolutionIndex].x;
        int height = (int)_settings.ScreenOption.Resolutions[_selectedResolutionIndex].y;
        Vector2 resolution = new Vector2(width, height);

        _settings.ScreenOption.SetSreenOptions(resolution, _fullScreen, _vSync);
        _settings.RefreshRates.SetActualRefreshRate(_refreshRateIndex);
    }

    private void ResolutionButtonLeft()
    {
        _selectedResolutionIndex--;

        UpdateResolutionText(_settings.ScreenOption.Resolutions[_selectedResolutionIndex]);

        _leftArrowResolutionUI.gameObject.SetActive(_selectedResolutionIndex != 0);
        _rightArrowResolutionUI.gameObject.SetActive(true);
    }

    private void ResolutionButtonRight()
    {
        _selectedResolutionIndex++;
        UpdateResolutionText(_settings.ScreenOption.Resolutions[_selectedResolutionIndex]);

        _rightArrowResolutionUI.gameObject.SetActive(_selectedResolutionIndex != _settings.ScreenOption.Resolutions.Count - 1);
        _leftArrowResolutionUI.gameObject.SetActive(true);
    }

    private void UpdateResolutionText(Vector2 screenResolution)
    {
        _resolutionUI.text = screenResolution.x.ToString() + " X " + screenResolution.y.ToString();
    }

    private void SetFullScreen(bool isFullScreen)
    {
        _fullScreen = isFullScreen;
    }

    private void SetVSync(bool isVSync)
    {
        _vSync = isVSync;
    }

    private void SetQuality(int qualityLevel)
    {
        _qualityValue = qualityLevel;
    }

    private void SetRefreshRate(int refreshRate)
    {
        _refreshRateIndex = refreshRate;
    }
}
