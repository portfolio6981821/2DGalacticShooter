﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "BoolVariable", menuName = "Game/Settings/BoolVariable")]
public class BoolSettingsVariable : ScriptableObject, ISettingsVariable
{
    public event Action<bool> OnValueChange;

    [SerializeField] private bool _defaultValue;
    [SerializeField] private bool _currentValue;

    public bool CurrentValue
    {
        get => _currentValue;
        set
        {
            _currentValue = value;
            OnValueChange?.Invoke(value);
        }
    }

    public bool DefaultValue => _defaultValue;
    public string SettingName => name;
}