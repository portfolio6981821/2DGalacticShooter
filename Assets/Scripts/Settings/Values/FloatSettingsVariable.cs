using System;
using UnityEngine;

[CreateAssetMenu(fileName = "NumberVariable", menuName = "Game/Settings/NumberVariable")]
[System.Serializable]
public class FloatSettingsVariable : ScriptableObject, ISettingsVariable
{
    public event Action<float> OnValueChange;

    [SerializeField] private float _defaultValue;
    [SerializeField] private float _minimumValue;
    [SerializeField] private float _maximumValue;

    [SerializeField] private float _currentValue;
    [SerializeField] private bool _onlyWholeNumbers;
    public float CurrentValue
    {
        get
        {
            CheckIfValueInRange(ref _currentValue);

            if (_onlyWholeNumbers)
            {
                _currentValue = (int)_currentValue;
            }

            return _currentValue;
        }

        set
        {
            CheckIfValueInRange(ref value);

            if (_onlyWholeNumbers)
            {
                value = (int)value;
            }

            _currentValue = value;
            OnValueChange?.Invoke(_currentValue);
        }
    }

    public float MinimumValue => _minimumValue;
    public float MaximumValue => _maximumValue;
    public bool OnlyWholeNumbers => _onlyWholeNumbers;
    public float DefaultValue => _defaultValue;
    public string SettingName => name;

    private void CheckIfValueInRange(ref float value)
    {
        if (value > MaximumValue)
        {
            value = MaximumValue;
        }

        if (value < MinimumValue)
        {
            value = MinimumValue;
        }
    }
}
