using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class HPImageReader : MonoBehaviour
{
    [SerializeField] private HPManager _hpToFollow;

    private Image _hpReader;
    private float _partialHPValue;

    private void Awake()
    {
        _partialHPValue = 1f / _hpToFollow.MaxHP;
        _hpReader = GetComponent<Image>();
        _hpReader.type = Image.Type.Filled;
        _hpReader.fillAmount = 1;

        _hpToFollow.OnGetHit += OnGetHit;
    }

    private void OnGetHit(int hp)
    {
        float value = _partialHPValue * hp;

        _hpReader.fillAmount = value;
    }
}
